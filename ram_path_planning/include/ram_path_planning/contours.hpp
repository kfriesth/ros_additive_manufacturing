#ifndef RAM_PATH_PLANNING_CONTOURS_HPP
#define RAM_PATH_PLANNING_CONTOURS_HPP

#include <ram_path_planning/donghong_ding_base.hpp>

namespace ram_path_planning
{
class Contours : public DonghongDingBase
{
public:

  Contours();

  std::string generateOneLayerTrajectory(const Polygon poly_data,
                                         Layer &layer,
                                         const double deposited_material_width,
                                         const std::array<double, 3> normal_vector = {0, 0, 1},
                                         const bool use_gui = false);

  std::string generateOneLayerTrajectory(const std::string yaml_file,
                                         Layer &layer,
                                         const double deposited_material_width,
                                         const bool use_gui = false);
};

}

#endif
