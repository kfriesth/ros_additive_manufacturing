#ifndef RAM_PATH_PLANNING_FOLLOWLINES_HPP
#define RAM_PATH_PLANNING_FOLLOWLINES_HPP

#include <future>
#include <mutex>

#include <vtkLine.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_path_planning/path_planning_algorithm.hpp>
#include <ram_utils/trajectory_files_manager.hpp>

namespace ram_path_planning
{
class FollowLines : public PathPlanningAlgorithm
{
public:
  // Polygon (possibly multiple contours)
  typedef vtkSmartPointer<vtkPolyData> Polygon;

  FollowLines();

  std::string generateTrajectory(const Polygon poly_data,
                                 ram_msgs::AdditiveManufacturingTrajectory &msg);

  std::string generateTrajectory(const std::string yaml_file,
                                 ram_msgs::AdditiveManufacturingTrajectory &msg);
};

}
#endif
