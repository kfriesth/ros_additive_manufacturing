#ifndef RAM_PATH_PLANNING_PATH_PLANNING_ALGORITHM_HPP
#define RAM_PATH_PLANNING_PATH_PLANNING_ALGORITHM_HPP

#include <ros/ros.h>

namespace ram_path_planning
{
class PathPlanningAlgorithm
{
public:
  PathPlanningAlgorithm(const std::string name,
                        const std::string description,
                        const std::string service_name);

  virtual ~PathPlanningAlgorithm()=0;

  const std::string name_;
  const std::string description_;
  const std::string service_name_;
};

}

#endif

