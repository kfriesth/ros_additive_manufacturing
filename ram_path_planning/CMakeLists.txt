cmake_minimum_required(VERSION 2.8.3)
project(ram_path_planning)
add_definitions(-std=c++11 -Wall -Wextra)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  eigen_conversions
  geometry_msgs
  message_generation
  message_runtime
  ram_msgs
  ram_utils
  roscpp
  roslib
  std_msgs
  tf_conversions
  unique_id
  uuid_msgs
  visualization_msgs
)

find_package(VTK 7.1.0 QUIET)
if (NOT VTK_FOUND)
  find_package(VTK 8.0.0 REQUIRED)
endif()
include(${VTK_USE_FILE})

################################################
## Declare ROS messages, services and actions ##
################################################

add_service_files(
  FILES
  GenerateTrajectoryContours.srv
  GenerateTrajectoryDonghongDing.srv
  GenerateTrajectoryFollowLines.srv
)

generate_messages(
  DEPENDENCIES
  ram_msgs
  std_msgs
)

###################################
## catkin specific configuration ##
###################################

catkin_package(
  INCLUDE_DIRS
  include
  LIBRARIES
  ${PROJECT_NAME}_contours
  ${PROJECT_NAME}_donghong_ding
  ${PROJECT_NAME}_donghong_ding_base
  ${PROJECT_NAME}_follow_lines
  ${PROJECT_NAME}_mesh_slicer
  CATKIN_DEPENDS
  geometry_msgs
  ram_msgs
  ram_utils
  std_msgs
  visualization_msgs
  DEPENDS
  VTK
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

# Library (mostly VTK) that slices a mesh into layers
add_library(
  ${PROJECT_NAME}_mesh_slicer
  src/mesh_slicer.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_mesh_slicer
  ${catkin_LIBRARIES}
  ${VTK_LIBRARIES}
)
add_dependencies(
  ${PROJECT_NAME}_mesh_slicer
  ${catkin_EXPORTED_TARGETS}
)

# Base library for all algorithms
add_library(
  ${PROJECT_NAME}_path_planning_algorithm
  src/path_planning_algorithm.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_path_planning_algorithm
  ${catkin_LIBRARIES}
)
add_dependencies(
  ${PROJECT_NAME}_path_planning_algorithm
  ${catkin_EXPORTED_TARGETS}
)

# Base library for Donhhong Ding alike algorithms (mostly VTK)
add_library(
  ${PROJECT_NAME}_donghong_ding_base
  src/donghong_ding_base.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_donghong_ding_base
  ${PROJECT_NAME}_path_planning_algorithm
  ${catkin_LIBRARIES}
  ${VTK_LIBRARIES}
)
add_dependencies(
  ${PROJECT_NAME}_donghong_ding_base
  ${catkin_EXPORTED_TARGETS}
)

# Library that computes path on each layer, filling volume
add_library(
  ${PROJECT_NAME}_donghong_ding
  src/donghong_ding.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_donghong_ding
  ${PROJECT_NAME}_donghong_ding_base
  ${catkin_LIBRARIES}
  ${VTK_LIBRARIES}
)
add_dependencies(
  ${PROJECT_NAME}_donghong_ding
  ${catkin_EXPORTED_TARGETS}
)

# Library that computes path on each layer, only the countour
add_library(
  ${PROJECT_NAME}_contours
  src/contours.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_contours
  ${PROJECT_NAME}_donghong_ding_base
  ${catkin_LIBRARIES}
  ${VTK_LIBRARIES}
)
add_dependencies(
  ${PROJECT_NAME}_contours
  ${catkin_EXPORTED_TARGETS}
)

# Library that computes path, follow the lines from a YAML file
add_library(
  ${PROJECT_NAME}_follow_lines
  src/follow_lines.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_follow_lines
  ${PROJECT_NAME}_path_planning_algorithm
  ${catkin_LIBRARIES}
  ${VTK_LIBRARIES}
)
add_dependencies(
  ${PROJECT_NAME}_follow_lines
  ${catkin_EXPORTED_TARGETS}
)

# Service node
add_executable(
  ${PROJECT_NAME}
  src/path_planning.cpp
)
target_link_libraries(
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
  ${VTK_LIBRARIES}
  ${PROJECT_NAME}_donghong_ding
  ${PROJECT_NAME}_contours
  ${PROJECT_NAME}_follow_lines
  ${PROJECT_NAME}_mesh_slicer
)
add_dependencies(
  ${PROJECT_NAME}
  ${PROJECT_NAME}_generate_messages_cpp
  ${catkin_EXPORTED_TARGETS}
)

#############
## Install ##
#############

## Mark executables and/or libraries for installation
install(TARGETS
  ${PROJECT_NAME}_donghong_ding_base
  ${PROJECT_NAME}_donghong_ding
  ${PROJECT_NAME}_contours
  ${PROJECT_NAME}_follow_lines
  ${PROJECT_NAME}_mesh_slicer
  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
)

## Mark other files for installation (e.g. launch and bag files, etc.)
install(DIRECTORY launch DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})
install(DIRECTORY meshes DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}) # Make sure meshes are not too heavy
install(DIRECTORY yaml DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})

#############
## Testing ##
#############

if(CATKIN_ENABLE_TESTING)
  find_package(rostest REQUIRED)
  add_rostest_gtest(
    ${PROJECT_NAME}_services_clients
    test/services_clients.launch
    test/services_clients.cpp
  )
  target_link_libraries(
    ${PROJECT_NAME}_services_clients
    ${catkin_LIBRARIES}
    ${PROJECT_NAME}_donghong_ding_base
    ${PROJECT_NAME}_donghong_ding
    ${PROJECT_NAME}_contours
    ${PROJECT_NAME}_follow_lines
    ${PROJECT_NAME}_mesh_slicer
    yaml-cpp
    ${VTK_LIBRARIES}
  )
  add_dependencies(
    ${PROJECT_NAME}_services_clients
    ${PROJECT_NAME}
    ${PROJECT_NAME}_generate_messages_cpp
    ${catkin_EXPORTED_TARGETS}
  )
endif()
