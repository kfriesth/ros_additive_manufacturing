#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_path_planning/GenerateTrajectoryContours.h>
#include <ram_path_planning/GenerateTrajectoryDonghongDing.h>
#include <ram_path_planning/contours.hpp>
#include <ram_path_planning/donghong_ding.hpp>
#include <ros/package.h>
#include <ros/ros.h>

#include <gtest/gtest.h>

std::unique_ptr<ros::NodeHandle> nh;
ros::ServiceClient donghong_ding_client;
ros::ServiceClient contours_client;
bool use_gui;

TEST(TestSuite, testSrvExistence)
{
  ram_path_planning::DonghongDing donghong_ding;
  ram_path_planning::Contours contours;

  donghong_ding_client = nh->serviceClient<ram_path_planning::GenerateTrajectoryDonghongDing>(
      donghong_ding.service_name_);
  bool donghong_ding_exists(donghong_ding_client.waitForExistence(ros::Duration(1)));

  contours_client = nh->serviceClient<ram_path_planning::GenerateTrajectoryContours>(contours.service_name_);
  bool contours_exists(contours_client.waitForExistence(ros::Duration(1)));

  EXPECT_TRUE(donghong_ding_exists && contours_exists);

}
/// Donghong Ding Algorithm
/// YAML
TEST(TestSuite, testPolygonWithinternalContoursDongHongDing)
{
  // Donghong Ding Algorithm
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/2_polygons_4_contours.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 0.25e-3;
  srv.request.height_between_layers = 2e-3;
  srv.request.number_of_layers = 2;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testIntersectedPolygonsDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/polygon_intersection.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 0.25e-3;
  srv.request.height_between_layers = 0.001;
  srv.request.number_of_layers = 2;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && !srv.response.error_msg.empty()); // Should fail because polygons intersect
}

TEST(TestSuite, testConvexPolygonDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/round_rectangle.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 0.25e-3;
  srv.request.height_between_layers = 2e-3;
  srv.request.number_of_layers = 2;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testConcavePolygonBigDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/concave_4_contours_big.yaml";
  srv.request.deposited_material_width = 0.1;
  srv.request.contours_filtering_tolerance = 0.5e-3;
  srv.request.height_between_layers = 0.1;
  srv.request.number_of_layers = 2;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testConcavePolygonSmallDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/concave_4_contours_small.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 0.25e-5;
  srv.request.height_between_layers = 0.1;
  srv.request.number_of_layers = 2;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testStarDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/star.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 0.25e-3;
  srv.request.height_between_layers = 0.1;
  srv.request.number_of_layers = 2;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

/// MESHES

TEST(TestSuite, testSTLFileTwistedPyramidDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.stl";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 1e-3;
  srv.request.height_between_layers = 5e-3;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testPLYFileTwistedPyramidDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.ply";
  srv.request.deposited_material_width = 0.001;
  srv.request.contours_filtering_tolerance = 0.5 * 1e-3;
  srv.request.height_between_layers = 0.005;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testOBJFileTwistedPyramidDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.obj";
  srv.request.deposited_material_width = 0.001;
  srv.request.contours_filtering_tolerance = 0.005 * 1e-3;
  srv.request.height_between_layers = 0.005;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testPLYFileTwoTwistedPyramidsDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/two_twisted_pyramids.ply";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 0.25e-3;
  srv.request.height_between_layers = 5e-3;
  bool success(donghong_ding_client.call(srv));
  EXPECT_FALSE(success && srv.response.error_msg.empty()); // Not implemented yet!
}

TEST(TestSuite, testConeTruncatedDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/cone_truncated.ply";
  srv.request.deposited_material_width = 0.001;
  srv.request.contours_filtering_tolerance = 0.6 * 1e-3;
  srv.request.height_between_layers = 0.005;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testDomeDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/dome.ply";
  srv.request.deposited_material_width = 0.001;
  srv.request.contours_filtering_tolerance = 0.5 * 1e-3;
  srv.request.height_between_layers = 0.002;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testInversedPyramidDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/inversed_pyramid.ply";
  srv.request.deposited_material_width = 0.001;
  srv.request.contours_filtering_tolerance = 0;
  srv.request.height_between_layers = 0.005;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testSTLCubeNonZSlicingDongHongDing)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/cube.stl";
  srv.request.deposited_material_width = 1e-3;
  srv.request.contours_filtering_tolerance = 1e-3;
  srv.request.height_between_layers = 5e-3;
  // Slicing vector is not Z:
  srv.request.slicing_direction.x = -0.15;
  srv.request.slicing_direction.y = -0.2;
  srv.request.slicing_direction.z = 1;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

/// Donghong Ding Algorithm
/// YAML
TEST(TestSuite, testPolygonWithinternalContours)
{
  // Contours Algorithm
  ram_path_planning::GenerateTrajectoryContours srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/2_polygons_4_contours.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.height_between_layers = 2e-3;
  srv.request.number_of_layers = 2;
  bool success(contours_client.call(srv));
  EXPECT_TRUE(success && !srv.response.error_msg.empty());
}

TEST(TestSuite, testIntersectedPolygonsContours)
{
  ram_path_planning::GenerateTrajectoryContours srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/polygon_intersection.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.height_between_layers = 0.001;
  srv.request.number_of_layers = 2;
  bool success(contours_client.call(srv));
  EXPECT_TRUE(success && !srv.response.error_msg.empty()); // Should fail because polygons intersect
}

TEST(TestSuite, testConvexPolygonContours)
{
  ram_path_planning::GenerateTrajectoryContours srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/round_rectangle.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.height_between_layers = 2e-3;
  srv.request.number_of_layers = 2;
  bool success(contours_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testStarContours)
{
  ram_path_planning::GenerateTrajectoryContours srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/yaml/star.yaml";
  srv.request.deposited_material_width = 1e-3;
  srv.request.height_between_layers = 0.1;
  srv.request.number_of_layers = 2;
  bool success(contours_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

/// MESHES

TEST(TestSuite, testSTLFileTwistedPyramidContours)
{
  ram_path_planning::GenerateTrajectoryContours srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.stl";
  srv.request.deposited_material_width = 1e-3;
  srv.request.height_between_layers = 5e-3;
  bool success(contours_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testPLYFileTwoTwistedPyramidsContours)
{
  ram_path_planning::GenerateTrajectoryDonghongDing srv;
  srv.request.file = ros::package::getPath("ram_path_planning") + "/meshes/two_twisted_pyramids.ply";
  srv.request.deposited_material_width = 1e-3;
  srv.request.height_between_layers = 5e-3;
  bool success(donghong_ding_client.call(srv));
  EXPECT_TRUE(success && !srv.response.error_msg.empty()); // Not implemented yet!

  if (use_gui)
    ROS_ERROR_STREAM("PLEASE CLOSE THE VTK WINDOW TO TERMINATE");
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "path_planning_test_client");
  nh.reset(new ros::NodeHandle);

  nh->param<bool>("use_gui", use_gui, false);
  if (use_gui)
    ROS_ERROR_STREAM("Press 'enter' in the terminal to go to the next step");

  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
