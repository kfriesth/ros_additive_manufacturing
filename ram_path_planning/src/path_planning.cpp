#include <string>
#include <strings.h>

#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_path_planning/GenerateTrajectoryContours.h>
#include <ram_path_planning/GenerateTrajectoryDonghongDing.h>
#include <ram_path_planning/GenerateTrajectoryFollowLines.h>
#include <ram_path_planning/contours.hpp>
#include <ram_path_planning/donghong_ding.hpp>
#include <ram_path_planning/follow_lines.hpp>
#include <ram_path_planning/mesh_slicer.hpp>
#include <ram_path_planning/vtkRenderUpdaterTimer.hpp>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#include <unique_id/unique_id.h>

typedef ram_path_planning::DonghongDingBase::Polygon Polygon;
typedef ram_path_planning::DonghongDingBase::PolygonVector PolygonVector;
typedef ram_path_planning::DonghongDingBase::Layer Layer;

bool use_gui;

ros::Publisher traj_pub;

ram_path_planning::DonghongDing area_generator;
ram_path_planning::Contours contour_generator;
ram_path_planning::FollowLines follow_lines_generator;

//allow to visualize the generation of trajectory
vtkSmartPointer<vtkRendererUpdaterTimer> cb = vtkSmartPointer<vtkRendererUpdaterTimer>::New();

std::string fileExtension(const std::string full_path)
{
  size_t last_index = full_path.find_last_of("/");
  std::string file_name = full_path.substr(last_index + 1, full_path.size());

  last_index = file_name.find_last_of("\\");
  file_name = file_name.substr(last_index + 1, file_name.size());

  last_index = file_name.find_last_of(".");
  if (last_index == std::string::npos)
    return "";

  return file_name.substr(last_index + 1, file_name.size());
}

bool donghongDingAlgorithmCallback(ram_path_planning::GenerateTrajectoryDonghongDing::Request &req,
                                   ram_path_planning::GenerateTrajectoryDonghongDing::Response &res)
{
  // Verify parameters
  if (req.file.empty())
  {
    res.error_msg = "File name cannot be empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  if (req.height_between_layers <= 0)
  {
    res.error_msg = "Height between layers cannot be <= 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  if (req.deposited_material_width <= 0)
  {
    res.error_msg = "Deposited material width cannot be <= 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }
  if (req.contours_filtering_tolerance < 0)
  {
    res.error_msg = "Contours filtering tolerance cannot be < 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  // Verify file extension
  bool is_yaml_file(false);
  const std::string file_extension(fileExtension(req.file));
  if (!strcasecmp(file_extension.c_str(), "yaml") || !strcasecmp(file_extension.c_str(), "yml"))
  {
    is_yaml_file = true;
    if (req.number_of_layers < 1)
    {
      res.error_msg = "Number of layers cannot be < 1";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }
  else if (strcasecmp(file_extension.c_str(), "obj") && strcasecmp(file_extension.c_str(), "ply")
      && strcasecmp(file_extension.c_str(), "stl"))
  {
    res.error_msg = "File is not a YAML, YML, OBJ, PLY or STL file: " + req.file;
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  // Create Trajectory message
  ram_msgs::AdditiveManufacturingTrajectory msg;
  msg.file = req.file;

  // Add request parameter to the trajectory
  msg.generation_info =
      "Deposited material width = " + std::to_string(req.deposited_material_width * 1000.0) + " mm\n";
  msg.generation_info +=
      "Contours filtering tolerance = " + std::to_string(req.contours_filtering_tolerance * 1000.0) + " mm\n";
  msg.generation_info +=
      "Height between layers = " + std::to_string(req.height_between_layers * 1000.0) + " mm\n";

  if (!is_yaml_file)
  {
    msg.generation_info +=
        "Slicing direction = " + std::to_string(req.slicing_direction.x) + ", " +
            std::to_string(req.slicing_direction.y) + ", " +
            std::to_string(req.slicing_direction.z);
  }
  else
  {
    msg.generation_info +=
        "Number of layers = " + std::to_string(req.number_of_layers);
  }

  msg.similar_layers = is_yaml_file;

  std::array<double, 3> slicing_direction;
  slicing_direction[0] = req.slicing_direction.x;
  slicing_direction[1] = req.slicing_direction.y;
  slicing_direction[2] = req.slicing_direction.z;

  std::vector<Layer> additive_traj;
  cb->current_layer_.clear();

  // Generate trajectory
  if (is_yaml_file)
  {
    std::string error_message;
    error_message = area_generator.generateOneLayerTrajectory(req.file, cb->current_layer_,
                                                              req.deposited_material_width,
                                                              req.contours_filtering_tolerance,
                                                              M_PI / 6,
                                                              false, use_gui);
    if (error_message.empty())
    {
      area_generator.connectYamlLayers(cb->current_layer_, msg, req.number_of_layers,
                                       req.height_between_layers);
    }
    else
    {
      res.error_msg = error_message + "\n" + "generateOneLayerTrajectory: Error generating trajectory";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }
  else
  {
    // Slice the mesh
    const unsigned nb_layers(
        ram_path_planning::sliceMesh(additive_traj, req.file, cb->mesh_, cb->stripper_, req.height_between_layers,
                                     slicing_direction,
                                     use_gui));

    if (nb_layers < 1)
    {
      res.error_msg = "Slicing failed, zero slice generated";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }

    // Generate trajectory on each layer
    for (unsigned i(0); i < additive_traj.size(); ++i)
    {
      Polygon contours(additive_traj[i][0][0]);
      contours->DeepCopy(additive_traj[i][0][0]);

      std::string error_message;
      error_message = area_generator.generateOneLayerTrajectory(contours, cb->current_layer_,
                                                                req.deposited_material_width,
                                                                req.contours_filtering_tolerance,
                                                                slicing_direction,
                                                                M_PI / 6, false, use_gui);
      if (!error_message.empty())
      {
        res.error_msg = error_message + "\n" + "Could not generate layer " + std::to_string(i);
        ROS_ERROR_STREAM(res.error_msg);
        return true;
      }
      additive_traj[i] = cb->current_layer_;
    }

    std::string return_message;

    return_message = area_generator.connectMeshLayers(additive_traj, msg);

    if (!return_message.empty())
    {
      res.error_msg = return_message + "\n" + "Error connecting layers";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }

  // Trajectory is now complete
  // Create a message
  // Fill response and publish trajectory
  if (msg.poses.size() == 0)
  {
    res.error_msg = "Trajectory is empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }
  res.number_of_poses = msg.poses.size();

  // Add UUID
  for (auto &pose : msg.poses)
    pose.unique_id = unique_id::toMsg(unique_id::fromRandom());

  // Set the time to zero, the trajectory is incomplete at this stage
  // It will be published on trajectory_tmp, completed by fill_trajectory and then published on trajectory
  msg.generated.nsec = 0;
  msg.generated.sec = 0;
  msg.modified = msg.generated;

  if (traj_pub.getNumSubscribers() == 0)
    ROS_ERROR_STREAM("No subscriber on topic " << traj_pub.getTopic() << ": trajectory is lost");

  traj_pub.publish(msg);
  return true;
}

bool contoursAlgorithmCallback(ram_path_planning::GenerateTrajectoryContours::Request &req,
                               ram_path_planning::GenerateTrajectoryContours::Response &res)
{
  // Verify parameters
  if (req.file.empty())
  {
    res.error_msg = "File name cannot be empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  if (req.height_between_layers <= 0)
  {
    res.error_msg = "Height between layers cannot be <= 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  if (req.deposited_material_width <= 0)
  {
    res.error_msg = "Deposited material width cannot be <= 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  // Verify file extension
  bool is_yaml_file(false);
  const std::string file_extension(fileExtension(req.file));
  if (!strcasecmp(file_extension.c_str(), "yaml") || !strcasecmp(file_extension.c_str(), "yml"))
  {
    is_yaml_file = true;
    if (req.number_of_layers < 1)
    {
      res.error_msg = "Number of layers cannot be < 1";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }
  else if (strcasecmp(file_extension.c_str(), "obj") && strcasecmp(file_extension.c_str(), "ply")
      && strcasecmp(file_extension.c_str(), "stl"))
  {
    res.error_msg = "File is not a YAML, YML, OBJ, PLY or STL file: " + req.file;
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  // Create Trajectory message
  ram_msgs::AdditiveManufacturingTrajectory msg;
  msg.file = req.file;

  // Add request parameter to the trajectory
  msg.generation_info =
      "Deposited material width = " + std::to_string(req.deposited_material_width * 1000.0) + " mm\n";
  msg.generation_info +=
      "Height between layers = " + std::to_string(req.height_between_layers * 1000.0) + " mm\n";

  if (!is_yaml_file)
  {
    msg.generation_info +=
        "Slicing direction = " + std::to_string(req.slicing_direction.x) + ", " +
            std::to_string(req.slicing_direction.y) + ", " +
            std::to_string(req.slicing_direction.z);
  }
  else
  {
    msg.generation_info +=
        "Number of layers = " + std::to_string(req.number_of_layers);
  }

  msg.similar_layers = is_yaml_file;
  std::array<double, 3> slicing_direction;
  slicing_direction[0] = req.slicing_direction.x;
  slicing_direction[1] = req.slicing_direction.y;
  slicing_direction[2] = req.slicing_direction.z;

  std::vector<Layer> additive_traj;
  cb->current_layer_.clear();

  // Generate trajectory
  if (is_yaml_file)
  {
    std::string error_message;
    error_message = contour_generator.generateOneLayerTrajectory(req.file, cb->current_layer_,
                                                                 req.deposited_material_width,
                                                                 use_gui);
    if (error_message.empty())
    {
      contour_generator.connectYamlLayers(cb->current_layer_, msg, req.number_of_layers,
                                          req.height_between_layers);
    }
    else
    {
      res.error_msg = error_message + "\n" + "generateOneLayerTrajectory: Error generating trajectory";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }
  else
  {
    // Slice the mesh
    const unsigned nb_layers(
        ram_path_planning::sliceMesh(additive_traj, req.file, cb->mesh_, cb->stripper_, req.height_between_layers,
                                     slicing_direction,
                                     use_gui));

    if (nb_layers < 1)
    {
      res.error_msg = "Slicing failed, zero slice generated";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }

    // Generate trajectory on each layer
    for (unsigned i(0); i < additive_traj.size(); ++i)
    {
      Polygon contours(additive_traj[i][0][0]);
      contours->DeepCopy(additive_traj[i][0][0]);

      std::string error_message;
      error_message = contour_generator.generateOneLayerTrajectory(contours, cb->current_layer_,
                                                                   req.deposited_material_width,
                                                                   slicing_direction, use_gui);
      if (!error_message.empty())
      {
        res.error_msg = error_message + "\n" + "Could not generate layer " + std::to_string(i);
        ROS_ERROR_STREAM(res.error_msg);
        return true;
      }
      additive_traj[i] = cb->current_layer_;
    }

    std::string return_message;
    return_message = contour_generator.connectMeshLayers(additive_traj, msg);
    if (!return_message.empty())
    {
      res.error_msg = return_message + "\n" + "Error connecting layers";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }
  // Trajectory is now complete
  // Create a message
  // Fill response and publish trajectory
  if (msg.poses.size() == 0)
  {
    res.error_msg = "Trajectory is empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }
  res.number_of_poses = msg.poses.size();

  // Add UUID
  for (auto &pose : msg.poses)
    pose.unique_id = unique_id::toMsg(unique_id::fromRandom());

  // Set the time to zero, the trajectory is incomplete at this stage
  // It will be published on trajectory_tmp, completed by fill_trajectory and then published on trajectory
  msg.generated.nsec = 0;
  msg.generated.sec = 0;
  msg.modified = msg.generated;

  if (traj_pub.getNumSubscribers() == 0)
    ROS_ERROR_STREAM("No subscriber on topic " << traj_pub.getTopic() << ": trajectory is lost");

  traj_pub.publish(msg);
  return true;
}

bool followLinesAlgorithmCallback(ram_path_planning::GenerateTrajectoryFollowLines::Request &req,
                                  ram_path_planning::GenerateTrajectoryFollowLines::Response &res)
{
  if (req.file.empty())
  {
    res.error_msg = "File name cannot be empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  // Verify file extension
  const std::string file_extension(fileExtension(req.file));
  if (strcasecmp(file_extension.c_str(), "yaml") && strcasecmp(file_extension.c_str(), "yml"))
  {
    res.error_msg = "File is not a YAML, YML, " + req.file;
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  // Create Trajectory message
  ram_msgs::AdditiveManufacturingTrajectory msg;
  msg.file = req.file;

  // Generate trajectory
  std::string error_message;
  // error_message = contour_generator.generateOneLayerTrajectory();
  error_message = follow_lines_generator.generateTrajectory(req.file, msg);
  if (!error_message.empty())
  {
    res.error_msg = error_message + "\n" + "generateTrajectory: Error generating trajectory";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }
  // Trajectory is now complete
  // Create a message
  // Fill response and publish trajectory
  if (msg.poses.size() == 0)
  {
    res.error_msg = "Trajectory is empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }
  res.number_of_poses = msg.poses.size();

  // Add UUID
  for (auto &pose : msg.poses)
    pose.unique_id = unique_id::toMsg(unique_id::fromRandom());

  // Set the time to zero, the trajectory is incomplete at this stage
  // It will be published on trajectory_tmp, completed by fill_trajectory and then published on trajectory
  msg.generated.nsec = 0;
  msg.generated.sec = 0;
  msg.modified = msg.generated;

  if (traj_pub.getNumSubscribers() == 0)
    ROS_ERROR_STREAM("No subscriber on topic " << traj_pub.getTopic() << ": trajectory is lost");

  traj_pub.publish(msg);
  return true;

}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "path_planing");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // new services
  ros::ServiceServer service_1 = nh.advertiseService(area_generator.service_name_, donghongDingAlgorithmCallback);
  ros::ServiceServer service_2 = nh.advertiseService(contour_generator.service_name_, contoursAlgorithmCallback);
  ros::ServiceServer service_3 = nh.advertiseService(follow_lines_generator.service_name_,
                                                     followLinesAlgorithmCallback);

  traj_pub = nh.advertise<ram_msgs::AdditiveManufacturingTrajectory>("ram/trajectory_tmp", 1, true);

  nh.param<bool>("use_gui", use_gui, false);
  if (use_gui)
  {
    vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> render_window = vtkSmartPointer<vtkRenderWindow>::New();
    render_window->AddRenderer(renderer);
    render_window->SetSize(800, 600);

    vtkSmartPointer<vtkRenderWindowInteractor> render_window_interactor =
        vtkSmartPointer<vtkRenderWindowInteractor>::New();
    render_window_interactor->SetRenderWindow(render_window);
    render_window_interactor->Initialize();

    vtkSmartPointer<vtkInteractorStyleTrackballCamera> image_style =
        vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
    render_window_interactor->SetInteractorStyle(image_style);

    render_window_interactor->AddObserver(vtkCommand::TimerEvent, cb);
    render_window_interactor->CreateRepeatingTimer(500);

    render_window_interactor->Start();
  }
  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
