#include <ram_path_planning/path_planning_algorithm.hpp>

namespace ram_path_planning
{
PathPlanningAlgorithm::PathPlanningAlgorithm(const std::string name,
                                             const std::string description,
                                             const std::string service_name) :
        name_(name),
        description_(description),
        service_name_(service_name)
{
}

PathPlanningAlgorithm::~PathPlanningAlgorithm()
{
}

}
