#include <ram_path_planning/contours.hpp>

namespace ram_path_planning
{
Contours::Contours() :
        DonghongDingBase("Contours", "Generate layers contours", "ram/path_planning/generate_trajectory/contours")

{
  deposited_material_width_ = 0.002; // 2 millimeters
}

std::string Contours::generateOneLayerTrajectory(const Polygon poly_data,
                                                 Layer &layer,
                                                 const double deposited_material_width,
                                                 const std::array<double, 3> normal_vector,
                                                 const bool use_gui)
{
  if (!poly_data)
    return "generateOneLayerTrajectory: polydata is not initialized";

  if (poly_data->GetNumberOfPoints() == 0)
    return "generateOneLayerTrajectory: polydata is empty";

  normal_vector_[0] = normal_vector[0];
  normal_vector_[1] = normal_vector[1];
  normal_vector_[2] = normal_vector[2];

  vtkMath::Normalize(normal_vector_);

  if (normal_vector[0] == 0 && normal_vector[1] == 0 && normal_vector[2] == 0)
  {
    normal_vector_[0] = 0;
    normal_vector_[1] = 0;
    normal_vector_[2] = 1;
  }

  deposited_material_width_ = deposited_material_width;

  if (!removeDuplicatePoints(poly_data, deposited_material_width_ / 2)) // tolerance default
    return "Failed to remove duplicate points";

  if (!mergeColinearEdges(poly_data))
    return "Failed to merge colinear edges";

  if (intersectionBetweenContours(poly_data))
    return "Contours intersects";

  std::vector<int> level;
  std::vector<int> father;
  identifyRelationships(poly_data, level, father);

  for (auto i : level)
    if (i != 0)
      return "Cannot generate a contour in a figure with holes";

  if (!organizePolygonContoursInLayer(poly_data, level, father, layer))
    return "Failed to organize polygon contours in layer";

  for (auto polygons : layer)
    for (auto polydata : polygons)
      if (!offsetPolygonContour(polydata, deposited_material_width_ / 2.0))
        return "Error in deposited material width";

  if (use_gui)
  {
    std::string s;
    ROS_INFO_STREAM("Path generated on one layer");
    std::getline(std::cin, s);
  }
  return "";
}

std::string Contours::generateOneLayerTrajectory(const std::string yaml_file,
                                                 Layer &layer,
                                                 const double deposited_material_width,
                                                 const bool use_gui)
{
  // Prepare contours
  const Polygon poly_data = Polygon::New();
  if (!ram_utils::TrajectoryFilesManager::yamlFileToPolydata(yaml_file, poly_data))
    return "Could not parse the YAML file";

  std::array<double, 3> normal_vector = {0, 0, 1};

  layer.clear();
  return generateOneLayerTrajectory(poly_data, layer, deposited_material_width, normal_vector, use_gui);
}

}
