#include <ram_path_planning/follow_lines.hpp>

namespace ram_path_planning
{
FollowLines::FollowLines() :
        PathPlanningAlgorithm("Follow lines", "Generate a trajectory from a list of points",
                              "ram/path_planning/generate_trajectory/follow_lines")
{
}

std::string FollowLines::generateTrajectory(const Polygon poly_data,
                                            ram_msgs::AdditiveManufacturingTrajectory &msg)
{
  // A contour in the poly_data is a polygon in the ram_msg
  msg.poses.clear();

  vtkIdType n_contours = poly_data->GetNumberOfCells();

  if (n_contours == 0)
    return "YAML is empty";

  for (vtkIdType contour_id(0); contour_id < n_contours; ++contour_id)
  {
    // Contours the poly_data
    vtkIdType n_points = poly_data->GetCell(contour_id)->GetNumberOfPoints();
    if (n_contours == 0)
      return "Polygon in YAML file is empty";

    for (vtkIdType point_id(0); point_id < n_points; ++point_id)
    {
      // points in contour
      double p[3]; // Central point
      poly_data->GetCell(contour_id)->GetPoints()->GetPoint(point_id, p);
      //TODO: Add point p to msg
      ram_msgs::AdditiveManufacturingPose ram_pose;
      // Geometric pose
      ram_pose.pose.orientation.w = 1;

      ram_pose.pose.position.x = p[0];
      ram_pose.pose.position.y = p[1];
      ram_pose.pose.position.z = p[2];

      // Polygon_start and polygon_end
      if (point_id == 0)
        ram_pose.polygon_start = true;
      if ((point_id + 1) == n_points)
        ram_pose.polygon_end = true;

      msg.poses.push_back(ram_pose);
    }
  }
  return "";
}

std::string FollowLines::generateTrajectory(const std::string yaml_file,
                                            ram_msgs::AdditiveManufacturingTrajectory &msg)
{
  // Prepare contours
  const Polygon poly_data = Polygon::New();
  if (!ram_utils::TrajectoryFilesManager::yamlFileToPolydata(yaml_file, poly_data))
    return "Could not parse the YAML file";

  return generateTrajectory(poly_data, msg);
}

}
