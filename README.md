 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) ROS additive manufacturing
===

[![build status](https://gitlab.com/InstitutMaupertuis/ros_additive_manufacturing/badges/kinetic/build.svg)](https://gitlab.com/InstitutMaupertuis/ros_additive_manufacturing/commits/kinetic)

This repository is part of the [ROS-Industrial](http://wiki.ros.org/Industrial) program.

**R**OS **A**dditive **M**anufacturing (`RAM`) provides a set of tools (mostly algorithms and GUIs) to help end users create complex trajectories for additive manufacturing. The project is especially focused on printing metallic parts with industrial robots. This project contains multiple tools: path planning algorithm able to generate complex trajectories, visualisation tools, editing tools, etc.


For more information please visit the offical wiki page:
http://wiki.ros.org/ros_additive_manufacturing

# Project logo

![RAM logo](./documentation/icon_blue.png)
