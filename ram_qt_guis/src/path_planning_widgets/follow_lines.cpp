#include <ram_qt_guis/path_planning_widgets/follow_lines.hpp>

namespace ram_qt_guis
{
FollowLinesWidget::FollowLinesWidget()

{
  setObjectName("FollowLinesWidget");

  main_layout_ = new QVBoxLayout(this);

  QHBoxLayout *file = new QHBoxLayout;
  QPushButton *file_explorer = new QPushButton;
  file_explorer->setText("...");
  file_explorer->setMaximumSize(30, 30);
  file_ = new QLineEdit;
  file->addWidget(file_);
  file->addWidget(file_explorer);
  connect(file_explorer, SIGNAL(released()), this, SLOT(browseFiles()));

  // Main layout
  main_layout_->addWidget(new QLabel("YAML file:"));
  main_layout_->addLayout(file);
  main_layout_->addStretch(1);

  connect(file_, SIGNAL(textChanged(QString)), this, SIGNAL(valueChanged()));
}

FollowLinesWidget::~FollowLinesWidget()
{
}

void FollowLinesWidget::browseFiles()
{
  QString file_dir("");
  {
    QFileInfo file(file_->text());
    if (!file_->text().isEmpty() && file.dir().exists())
      file_dir = file.dir().path();
    else
    {
      std::string path = ros::package::getPath("ram_path_planning");
      file_dir = QString::fromStdString(path);
    }
  }

  QFileDialog browser;
  browser.setOption(QFileDialog::DontUseNativeDialog, true);
  QString file_path = browser.getOpenFileName(this, "Choose YAML file", file_dir,
                                              "YAML files (*.yaml *.yml)");
  if (file_path != "")
    file_->setText(file_path);
}

void FollowLinesWidget::load(const rviz::Config& config)
{
  QString tmp_str("");

  if (config.mapGetString(objectName() + "file", &tmp_str))
    file_->setText(tmp_str);
}

void FollowLinesWidget::save(rviz::Config config) const
                          {
  config.mapSetValue(objectName() + "file", file_->text());
}

void FollowLinesWidget::fillRequest(ram_path_planning::GenerateTrajectoryFollowLines::Request &req)
{
  req.file = file_->text().toStdString();
}

std::string FollowLinesWidget::fileExtension(const std::string full_path)
{
  size_t last_index = full_path.find_last_of("/");
  std::string file_name = full_path.substr(last_index + 1, full_path.size());

  last_index = file_name.find_last_of("\\");
  file_name = file_name.substr(last_index + 1, file_name.size());

  last_index = file_name.find_last_of(".");
  if (last_index == std::string::npos)
    return "";

  return file_name.substr(last_index + 1, file_name.size());
}

}
