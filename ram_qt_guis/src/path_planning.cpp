#include <ram_qt_guis/path_planning.hpp>

namespace ram_qt_guis
{

PathPlanning::PathPlanning(QWidget* parent) :
        rviz::Panel(parent)
{
  setObjectName("PathPlanning");
  setName(objectName());

  QHBoxLayout *select_algorithm_layout = new QHBoxLayout;
  select_algorithm_layout->addWidget(new QLabel("Generation algorithm:"));

  select_algorithm_ = new QComboBox;
  select_algorithm_layout->addWidget(select_algorithm_);

  algorithm_description_ = new QLabel;

  algorithm_stacked_widget_ = new QStackedWidget();

  generate_trajectory_button_ = new QPushButton("Generate trajectory");

  // Scroll area
  QVBoxLayout *scroll_widget_layout = new QVBoxLayout();
  QWidget *scroll_widget = new QWidget;
  scroll_widget->setLayout(scroll_widget_layout);
  QScrollArea *scroll_area = new QScrollArea;
  scroll_area->setWidget(scroll_widget);
  scroll_area->setWidgetResizable(true);
  scroll_area->setFrameShape(QFrame::NoFrame);

  // Main layout
  QVBoxLayout* main_layout = new QVBoxLayout(this);
  main_layout->addWidget(scroll_area);
  scroll_widget_layout->addLayout(select_algorithm_layout);
  scroll_widget_layout->addWidget(algorithm_description_);
  scroll_widget_layout->addStretch(1);
  scroll_widget_layout->addWidget(algorithm_stacked_widget_);
  scroll_widget_layout->addStretch(2);
  scroll_widget_layout->addWidget(generate_trajectory_button_);

  connect(this, SIGNAL(displayErrorMessageBox(const QString, const QString, const QString)), this,
          SLOT(displayErrorBoxHandler(const QString, const QString, const QString)));

  // Algorithm generators
  ram_path_planning::DonghongDing donghong_ding_generator;
  ram_path_planning::Contours contours_generator;
  ram_path_planning::FollowLines follow_lines_generator;

  // Algorithms services
  donghong_ding_client_ = nh_.serviceClient<ram_path_planning::GenerateTrajectoryDonghongDing>(
      donghong_ding_generator.service_name_);
  contours_client_ = nh_.serviceClient<ram_path_planning::GenerateTrajectoryContours>(contours_generator.service_name_);
  follow_lines_client_ = nh_.serviceClient<ram_path_planning::GenerateTrajectoryFollowLines>(
      follow_lines_generator.service_name_);

  // Add algorithms names
  select_algorithm_->addItem(QString::fromStdString(donghong_ding_generator.name_));
  select_algorithm_->addItem(QString::fromStdString(contours_generator.name_));
  select_algorithm_->addItem(QString::fromStdString(follow_lines_generator.name_));

  // Add algorithms descriptions
  algorithm_descriptions_.clear();
  algorithm_descriptions_.push_back(donghong_ding_generator.description_);
  algorithm_descriptions_.push_back(contours_generator.description_);
  algorithm_descriptions_.push_back(follow_lines_generator.description_);

  // Algorithm widget
  donghong_ding_ui_ = new DonghongDingWidget();
  contours_ui_ = new ContoursWidget();
  follow_lines_ui_ = new FollowLinesWidget();

  // Add algorithm widget
  algorithm_stacked_widget_->addWidget(donghong_ding_ui_);
  algorithm_stacked_widget_->addWidget(contours_ui_);
  algorithm_stacked_widget_->addWidget(follow_lines_ui_);

  connect(donghong_ding_ui_, SIGNAL(valueChanged()), this, SIGNAL(configChanged()));
  connect(contours_ui_, SIGNAL(valueChanged()), this, SIGNAL(configChanged()));
  connect(follow_lines_ui_, SIGNAL(valueChanged()), this, SIGNAL(configChanged()));

}

PathPlanning::~PathPlanning()
{
}

void PathPlanning::connectToService(ros::ServiceClient &client)
{
  while (nh_.ok())
  {
    if (client.waitForExistence(ros::Duration(2)))
    {
      ROS_INFO_STREAM(
                      "RViz panel " << getName().toStdString() << " connected to the service " << client.getService());
      break;
    }
    else
    {
      ROS_ERROR_STREAM(
          "RViz panel " << getName().toStdString() << " could not connect to ROS service: " << client.getService());
      ros::Duration(1).sleep();
    }
  }
}

void PathPlanning::connectToServices()
{
  Q_EMIT setEnabled(false);
  // Generate trajectory
  connectToService(donghong_ding_client_);
  connectToService(contours_client_);
  connectToService(follow_lines_client_);

  ROS_INFO_STREAM("RViz panel " << getName().toStdString() << " services connections have been made");

  connect(select_algorithm_, SIGNAL(currentIndexChanged(int)), this, SLOT(algorithmChanged()));
  connect(select_algorithm_, SIGNAL(currentIndexChanged(int)), this, SIGNAL(configChanged()));

  select_algorithm_->setCurrentIndex(loaded_last_algorithm_);
  Q_EMIT select_algorithm_->currentIndexChanged(loaded_last_algorithm_);
  Q_EMIT setEnabled(true);
}

void PathPlanning::algorithmChanged()
{
  Q_EMIT setEnabled(false);

  generate_trajectory_button_->disconnect();

  // Change description
  algorithm_description_->setText(QString::fromStdString(algorithm_descriptions_[select_algorithm_->currentIndex()]));
  // Change widget
  algorithm_stacked_widget_->setCurrentIndex(select_algorithm_->currentIndex());

  // Connect button
  switch (select_algorithm_->currentIndex())
  {
    case 0: // Donghong Ding Algorithm
    {
      connect(generate_trajectory_button_, SIGNAL(clicked()), this, SLOT(donghongDingButtonHandler()));
      break;
    }
    case 1: // Contours Algorithm
    {
      connect(generate_trajectory_button_, SIGNAL(clicked()), this, SLOT(contoursButtonHandler()));
      break;
    }
    case 2: // Follow Lines Algorithm
    {
      connect(generate_trajectory_button_, SIGNAL(clicked()), this, SLOT(followLinesButtonHandler()));
      break;
    }

    default:
      displayErrorBoxHandler("Operation", "Error selecting algorithm", "");
      break;
  }
  Q_EMIT setEnabled(true);
}

void PathPlanning::donghongDingButtonHandler()
{
  Q_EMIT configChanged();
  Q_EMIT setEnabled(false);

  donghong_ding_ui_->fillRequest(donghong_ding_params_.request);

  // Run in a separate thread
  QFuture<void> future = QtConcurrent::run(this, &PathPlanning::sendDonghongDingInformation);
}

void PathPlanning::contoursButtonHandler()
{
  Q_EMIT configChanged();
  Q_EMIT setEnabled(false);

  contours_ui_->fillRequest(contours_params_.request);

  // Run in a separate thread
  QFuture<void> future = QtConcurrent::run(this, &PathPlanning::sendContoursInformation);
}

void PathPlanning::followLinesButtonHandler()
{
  Q_EMIT configChanged();
  Q_EMIT setEnabled(false);

  follow_lines_ui_->fillRequest(follow_lines_params_.request);

  // Run in a separate thread
  QFuture<void> future = QtConcurrent::run(this, &PathPlanning::sendFollowLinesInformation);
}

void PathPlanning::sendDonghongDingInformation()
{
  bool success(donghong_ding_client_.call(donghong_ding_params_));

  if (!success)
  {
    Q_EMIT displayErrorMessageBox("Service call failed",
                                  QString::fromStdString(donghong_ding_client_.getService()),
                                  "Check the logs!");
    Q_EMIT setEnabled(true);
    return;
  }

  if (!donghong_ding_params_.response.error_msg.empty())
  {
    Q_EMIT displayErrorMessageBox("Path planning failed",
                                  QString::fromStdString(donghong_ding_params_.response.error_msg),
                                  "");
    Q_EMIT setEnabled(true);
    return;
  }

  Q_EMIT setEnabled(true);
}

void PathPlanning::sendContoursInformation()
{
  bool success(contours_client_.call(contours_params_));

  if (!success)
  {
    Q_EMIT displayErrorMessageBox("Service call failed",
                                  QString::fromStdString(contours_client_.getService()),
                                  "Check the logs!");
    Q_EMIT setEnabled(true);
    return;
  }

  if (!contours_params_.response.error_msg.empty())
  {
    Q_EMIT displayErrorMessageBox("Path planning failed",
                                  QString::fromStdString(contours_params_.response.error_msg),
                                  "");
    Q_EMIT setEnabled(true);
    return;
  }

  Q_EMIT setEnabled(true);
}

void PathPlanning::sendFollowLinesInformation()
{
  bool success(follow_lines_client_.call(follow_lines_params_));

  if (!success)
  {
    Q_EMIT displayErrorMessageBox("Service call failed",
                                  QString::fromStdString(follow_lines_client_.getService()),
                                  "Check the logs!");
    Q_EMIT setEnabled(true);
    return;
  }

  if (!follow_lines_params_.response.error_msg.empty())
  {
    Q_EMIT displayErrorMessageBox("Path planning failed",
                                  QString::fromStdString(follow_lines_params_.response.error_msg),
                                  "");
    Q_EMIT setEnabled(true);
    return;
  }

  Q_EMIT setEnabled(true);
}

void PathPlanning::displayErrorBoxHandler(const QString title,
                                          const QString message,
                                          const QString info_msg)
{
  const bool old_state(isEnabled());
  Q_EMIT setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(message);
  msg_box.setInformativeText(info_msg);
  msg_box.setIcon(QMessageBox::Critical);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  Q_EMIT setEnabled(old_state);
}

void PathPlanning::load(const rviz::Config& config)
{
  Q_EMIT configChanged();

  int tmp_int(0);
  if (config.mapGetInt("algorithm", &tmp_int))
    loaded_last_algorithm_ = tmp_int;

  donghong_ding_ui_->load(config);
  contours_ui_->load(config);
  follow_lines_ui_->load(config);
  rviz::Panel::load(config);

  // Check connection of client
  QFuture<void> future = QtConcurrent::run(this, &PathPlanning::connectToServices);
}

void PathPlanning::save(rviz::Config config) const
                        {
  config.mapSetValue("algorithm", select_algorithm_->currentIndex());

  donghong_ding_ui_->save(config);
  contours_ui_->save(config);
  follow_lines_ui_->save(config);
  rviz::Panel::save(config);
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ram_qt_guis::PathPlanning, rviz::Panel)
