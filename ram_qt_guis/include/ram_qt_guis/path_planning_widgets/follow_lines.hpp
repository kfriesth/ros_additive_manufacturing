#ifndef RAM_QT_GUIS_ALGORITHMS_WIDGETS_FOLLOW_LINES_WIDGET_HPP
#define RAM_QT_GUIS_ALGORITHMS_WIDGETS_FOLLOW_LINES_WIDGET_HPP

#include <mutex>

#include <ram_path_planning/GenerateTrajectoryFollowLines.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <rviz/panel.h>

#include <QCheckBox>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QSpinBox>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

namespace ram_qt_guis
{
class FollowLinesWidget : public QWidget
{
Q_OBJECT
  public:
  FollowLinesWidget();
  virtual ~FollowLinesWidget();

  void load(const rviz::Config& config);
  void save(rviz::Config config) const;

  void fillRequest(ram_path_planning::GenerateTrajectoryFollowLinesRequest &req);

Q_SIGNALS:
  void valueChanged();

protected Q_SLOTS:
  void browseFiles();

private:
  std::string fileExtension(const std::string full_path);
  QVBoxLayout *main_layout_;
  QLineEdit *file_;
};

}

#endif
