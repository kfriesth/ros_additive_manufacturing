#ifndef RAM_QT_GUIS_PATH_PLANNING_HPP
#define RAM_QT_GUIS_PATH_PLANNING_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <ram_path_planning/contours.hpp>
#include <ram_path_planning/donghong_ding.hpp>
#include <ram_path_planning/follow_lines.hpp>
#include <ram_qt_guis/path_planning_widgets/contours.hpp>
#include <ram_qt_guis/path_planning_widgets/donghong_ding.hpp>
#include <ram_qt_guis/path_planning_widgets/follow_lines.hpp>
#include <ros/package.h>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFuture>
#include <QHBoxLayout>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QStackedWidget>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class PathPlanning : public rviz::Panel
{
Q_OBJECT

public:
  PathPlanning(QWidget* parent = NULL);
  virtual ~PathPlanning();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

protected Q_SLOTS:
  void addAlgorithmsToGUI();
  void algorithmChanged();

  void donghongDingButtonHandler();
  void sendDonghongDingInformation();

  void contoursButtonHandler();
  void sendContoursInformation();

  void followLinesButtonHandler();
  void sendFollowLinesInformation();

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

  void load(const rviz::Config& config);
  void save(rviz::Config config) const;

private:
  void connectToServices();
  void connectToService(ros::ServiceClient &client);

  ros::NodeHandle nh_;

  QStackedWidget * algorithm_stacked_widget_;
  QPushButton *generate_trajectory_button_;

  QComboBox *select_algorithm_;
  QLabel *algorithm_description_;
  std::vector<std::string> algorithm_descriptions_;
  unsigned loaded_last_algorithm_ = 0;

  //DonghongDing Algorithm
  DonghongDingWidget *donghong_ding_ui_;
  ros::ServiceClient donghong_ding_client_;
  ram_path_planning::GenerateTrajectoryDonghongDing donghong_ding_params_;

  //Contours Algorithm
  ContoursWidget *contours_ui_;
  ros::ServiceClient contours_client_;
  ram_path_planning::GenerateTrajectoryContours contours_params_;

  // Follow Lines Algorithm
  FollowLinesWidget *follow_lines_ui_;
  ros::ServiceClient follow_lines_client_;
  ram_path_planning::GenerateTrajectoryFollowLines follow_lines_params_;

};

}

#endif
