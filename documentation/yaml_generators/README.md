# YAML generators
This directory contains simple C++ programs (ROS is not a dependency) to generate YAML files.

The program `display_yaml` allows to visualize a YAML file. It can be any YAML file that complies to the following format: (eg: a triangle)

```yaml
---
layer:
  -
    polygon:
    - point: [0.2, 0, 0]
    - point: [0.0, 0.2, 0]
    - point: [0, 0, 0]
```

These files can then be used within ROS Additive Manufacturing.
